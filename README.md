# Task32

- Convert the login and register forms in your previous task use Reactive Forms
- Setup validation for each input
  - Required
  - Minimum Length

## info on the project

The app contains 3 components:

1. Register component
2. Login component
3. Dashboard component

Below follows info on each of them individually

## 1. Register component

The register form contains 3 fields: username, password and confirm password. For a new user to register the criteria for each of these fields have to be met

- Username: Between 8 and 20 characters

- Password: Between 9 and 20 characters including both upper-and lowercase letters as well as at least one number and a special character (e.g. %)

- Confirm password has to match the password

If the register is successful the user is redirected to the dashboard.

In the bottom of the form there is a link to the login form in case the user already has created an account. 

## 2. Login component

The login form contains a username and a password field for the user to login to the dashboard. If they match an existing user's credentials then access to the dashboard is granted.

Morever, a session is created after the login is successful so an eventual attempt to access the other routes (/register or / ) will also redirect the user to the dashboard.

In the bottom of the form there is a link to the register form in case the user does not have an account. 

## 3. Dashboard component

The dashboard component welcomes the user after a successful register or login. 

A logout button exists in case the user wants to log out from the service. It redirects to the login form.

