import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  [x: string]: any;

  //private user: any = null;
  //private userLogin: any = null;

  constructor(private http: HttpClient) { }

  register(user): Promise<any> {
    return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/register', {
      user: {
        username: user.username,
        password:user.password
      }
    }).toPromise()
  }

 /*  login(userLogin): Promise<any> {
    return this.http.get('https://survey-poodle.herokuapp.com/v1/api/users', { params: Array
    }).toPromise()

  } */

  login(user): Promise<any> {
    return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/login', {
      user: {...user
      }
    }).toPromise()
  }

 /*  public login(userLogin: any): boolean {
    if (userLogin.username === 'mihalis1' && userLogin.password == "secretooo") {
      this.userLogin = {...userLogin};
    } else {
      this.userLogin = null;
    }
    return this.isLoggedIn();
  } */

  public isLoggedIn(): boolean {
    return this.userLogin !== null;
  }
}
